INTRODUCTION
------------

CDNetworks Purge allows for Tag, URL and Regular Expression URL based
invalidation. A CDNetworks username and an API key with permissions to relevant
Content Management APIs are required.

REQUIREMENTS
------------

As part of the composer based installation two modules will be deployed as
dependencies:
 * The Purge module to facilitate cleaning external caching systems, reverse
 proxies and CDNs as content actually changes, see
 https://www.drupal.org/project/purge
 * the Key module to provide the framework for securely store senistive
 API keys, see https://www.drupal.org/project/key.

### Composer
If your site is [managed via Composer](https://www.drupal.org/node/2718229),
use Composer to download the module, which will also download the required
modules as dependencies:
   ```sh
   composer require "drupal/cdnetworks_purge ~1.0"
   ```
~1.0 downloads the latest release, use 1.x-dev to get the -dev release instead.
Use ```composer update drupal/cdnetworks_purge --with-dependencies``` to update
to a new release.

INSTALLATION
-------------

 * Enable like any Drupal module:
 https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
 * Enable Purge Queuers, Core tags queuer module as well for cache tag purge.
 * Make sure Purge UI module is enabled.

CONFIGURATION
-------------

1. Get API credentials from CDNetworks and set the keys in the CDNetworks Purge Settings
2. There is an option Enable Cache Tag Headers and how many items the system can invalidate at a time
3. After enabling the module set the necessary configuration at
 /admin/config/development/cdnetworks_purge
4. Test some API calls using the manual purge form:
 /admin/config/development/cdnetworks_purge/purge
5. Configure Purge module by adding the CDNetworks purger
6. By default, this is using the purge_queuer_coretags module provided by Purge.
If you wanted to purge by URL, you would need to implement other methods for
queueing URLs such as https://www.drupal.org/project/purge_queuer_url

### API Client
The basis of a CDNetworks API client is included and can be implemented as a
service in your custom code.

TROUBLESHOOTING
---------------

 * All errors are logged to watchdog.

## D9 compatibility
Our code for CDNetworks purge module is fully D9 compatible.
As this module depends on other modules Purge and Keys, please keep an eye for
their compatibility with D9. Open issues for Purge:
https://www.drupal.org/project/purge/issues/3087394
