<?php

namespace Drupal\cdnetworks_purge\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\cdnetworks_purge\Client\CdnetworksPurgeApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CachePurgeForm.
 */
class CachePurgeForm extends FormBase {

  /**
   * CDNetworks API Client.
   *
   * @var \Drupal\cdnetworks_purge\Client\CdnetworksPurgeApiClient
   */
  protected $client;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The constructor.
   *
   * @param \Drupal\cdnetworks_purge\Client\CdnetworksPurgeApiClient $client
   *   The CdnetworksPurgeApiClient.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The MessengerInterface.
   */
  public function __construct(CdnetworksPurgeApiClient $client, MessengerInterface $messenger) {
    $this->client = $client;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('cdnetworks_purge.client'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cdnetworks_purge_cache_purge_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['urls'] = [
      '#type' => 'details',
      '#title' => $this->t('URL Purging'),
      '#open' => TRUE,
    ];

    $form['urls']['urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('URLs to Purge'),
      '#description' => $this->t('<ul><li>Separate each url with a return. Directory (wildcard) URLs end in a /. </li><li>The URL must start with http:// or https://</li><li>Each url has a maximum length of 1000 characters.</li><li>The domain in the URL must be the domain of the CDN service.</li></>'),
      '#rows' => 12,
      '#weight' => 1,
    ];
    $options = [
      'default' => $this->t('Default: the pre-configured operation type of domain'),
      'delete' => $this->t('Delete: directly delete the cache file of the submitted url'),
      'expire' => $this->t('Expire: set the file with the cached commit url to expire'),
    ];

    $form['urls']['url_action'] = [
      '#type' => 'radios',
      '#title' => $this->t('URL Action'),
      '#options' => $options,
      '#description' => $this->t('Used if any URLs (not ending in a /) exist.'),
      '#default_value' => 'default',
      '#weight' => 2,
    ];
    $form['urls']['dir_action'] = [
      '#type' => 'radios',
      '#title' => $this->t('Directory Action'),
      '#options' => $options,
      '#description' => $this->t('Used if any Directory URLs (ending in a /) exist.'),
      '#default_value' => 'default',
      '#weight' => 3,
    ];

    $form['urls_regex'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Regex URLs to Purge'),
      '#description' => $this->t('<ul><li>Separate each url with a return.</li><li>The URL conforms to the regular expression. Enter the example: http://www.a.com/(.*).png. To push the http://www.abc.com/test/.*\.txt regular expression, you need to escape the backslash when using the interface, ie http://www.abc.com/test/. *\\.TXT.</li><li>The domain name of each regular url must be the domain name accelerated by our company.</li><li>The default is 500 strips/day, which is the upper limit shared with the directory push.</li></>'),
      '#rows' => 12,
      '#weight' => 4,
    ];

    $form['tags'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Tags to Purge'),
      '#description' => $this->t('<ul><li>Separate each tag with a return.</li><li>Examples: node_1, paragraph_5, node_list</li></>'),
      '#rows' => 12,
      '#weight' => 5,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Purge URLs'),
      '#weight' => 20,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('urls'))) {
      $urls = preg_split("/\r\n|\n|\r/", $form_state->getValue('urls'));
      $client = $this->client->purgeUrl($urls, $form_state->getValue('url_action'), $form_state->getValue('dir_action'));
      if ($client) {
        $this->messenger->addMessage($this->t('URLs were purged.'));
      }
    }

    if (!empty($form_state->getValue('urls_regex'))) {
      $urls = preg_split("/\r\n|\n|\r/", $form_state->getValue('urls_regex'));
      if ($this->client->purgeRegex($urls)) {
        $this->messenger->addMessage($this->t('The regular expression URLs were purged.'));
      }
    }

    if (!empty($form_state->getValue('tags'))) {
      $tags = preg_split("/\r\n|\n|\r/", $form_state->getValue('tags'));
      // Send one at a time.
      foreach ($tags as $tag) {
        if ($this->client->purgeTag($tag)) {
          $this->messenger->addMessage($this->t('Tag @tag was purged.', ['@tag' => $tag]));
        }
      }
    }
  }

}
