<?php

namespace Drupal\cdnetworks_purge\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Build CDNetworks admin form UI.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cdnetworks_purge_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfig();

    $form['username'] = [
      '#type' => 'key_select',
      '#title' => $this->t('CDNetworks Username'),
      '#default_value' => $config->get('username'),
    ];

    $form['apikey'] = [
      '#type' => 'key_select',
      '#title' => $this->t('CDNetworks APIkey'),
      '#default_value' => $config->get('apikey'),
    ];

    $form['cachetag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Cache Tag headers'),
      '#default_value' => $config->get('cachetag'),
      '#description' => $this->t('Select if you want to generate Cache Tag header for CDNetworks'),
    ];

    $form['verbose_log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Verbose logging'),
      '#default_value' => $config->get('verbose_log'),
      '#description' => $this->t('Select if you want to provide verbose logging'),
    ];

    $form['ideal_conditions_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Ideal Conditions Limit'),
      '#default_value' => $config->get('ideal_conditions_limit'),
      '#description' => $this->t('Optional: This limit is used to control how many items the system can invalidate at a time.'),
    ];

    $form['cdn_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Accelerated CDNetworks domain'),
      '#default_value' => $config->get('cdn_url'),
      '#description' => $this->t('Do not include http:// or https://. Ex. www.example.com'),
    ];

    $form['base_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDNetworks Base REST URI'),
      '#default_value' => $config->get('base_uri'),
      '#description' => $this->t('Do not inclue trailing slash. Ex. https://api.cdnetworks.com'),
    ];

    return parent::buildForm($form, $form_state);
  }

}
