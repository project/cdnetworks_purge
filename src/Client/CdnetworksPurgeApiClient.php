<?php

namespace Drupal\cdnetworks_purge\Client;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\key\KeyRepositoryInterface;
use Drupal\cdnetworks_purge\CdnetworksPurgeApiClientInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;
use Drupal\Component\Serialization\Json;

/**
 * Define CDNetworks class.
 */
class CdnetworksPurgeApiClient implements CdnetworksPurgeApiClientInterface {
  use StringTranslationTrait;

  /**
   * The Immutable Config Object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * An http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The CDNetworks logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The KeyRepositoryInterface.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepo;

  /**
   * CDNetworks Username.
   *
   * @var string
   */
  protected $username;

  /**
   * CDNetworks Password.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * CDNetworks Date.
   *
   * @var string
   */
  protected $date;

  /**
   * CDNetworks Base URI.
   *
   * @var string
   */
  protected $baseUri;

  /**
   * The constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The ClientInterface.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The KeyRepositoryInterface.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The ConfigFactoryInterface.
   * @param \Psr\Log\LoggerInterface $logger
   *   The LoggerInterface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The MessengerInterface.
   */
  public function __construct(
    ClientInterface $http_client,
    KeyRepositoryInterface $key_repository,
    ConfigFactoryInterface $config_factory,
    LoggerInterface $logger,
    MessengerInterface $messenger
    ) {
    $this->httpClient = $http_client;
    $this->keyRepo = $key_repository;
    $this->config = $config_factory->get('cdnetworks_purge.settings');
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->username = $this->getKeyValue('username');
    $this->apiKey = $this->getKeyValue('apikey');
    $this->baseUri = $this->config->get('base_uri');
    $this->date = $this->getDate();
  }

  /**
   * Get Regex URLs from the admin interface.
   */
  public function purgeRegex(array $urls) {
    if (empty($urls)) {
      $this->logger->error('No URLs were passed to purgeRegex.');
      return FALSE;
    }

    $body = [
      'urlRegulars' => $urls,
    ];
    $options = $this->prepareOptions($body);
    if ($this->request('/api/content/regular-url/purge', $options)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get Tags from the admin interface.
   */
  public function purgeTag($tag, $action = 0) {
    // TODO: Find out if $tag is a single request or not.
    // Docs look like a comma seperated list.
    if (empty($tag)) {
      $this->logger->error('No Tags were passed to purgeTag.');
      return FALSE;
    }

    // Replace special character not allowed on cahe tags.
    $tagRepl = str_replace(':', '_', $tag);

    $body = [
      'tag' => $tagRepl,
      'action' => $action,
    ];

    $options = $this->prepareOptions($body);
    if ($this->request('/api/content/tag/purge', $options)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get individual URLs to be purged from the admin interface.
   */
  public function purgeUrl(array $urls, $urlAction = 'default', $dirAction = 'default') {
    // TODO: Pass $paths through a validator.
    // TODO: frequencyCheck -- Call frequency: 10/5min.
    if (empty($urls)) {
      $this->logger->error('No URLs were passed to purgeURL.');
      return FALSE;
    }

    $body = [];
    // Group URLs into directories and normal urls.
    $groupedUrls = $this->groupUrls($urls);

    if (isset($groupedUrls['urls']) && !empty($groupedUrls['urls'])) {
      $body['urls'] = $groupedUrls['urls'];
      $body['urlAction'] = $urlAction;
    }
    if (isset($groupedUrls['dirs']) && !empty($groupedUrls['dirs'])) {
      $body['dirs'] = $groupedUrls['dirs'];
      $body['dirAction'] = $dirAction;
    }

    // Make sure these are not empty after grouping.
    if (isset($body['urls']) || isset($body['dirs'])) {
      $options = $this->prepareOptions($body);
      if ($this->request('/ccm/purge/ItemIdReceiver', $options)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Group urls by type.
   *
   * @param array $urls
   *   An array of urls to group.
   *
   * @return array
   *   Multi-dimensional array of urls grouped by type.
   */
  private function groupUrls(array $urls) {
    $urlsGrouped = [];
    foreach ($urls as $url) {
      // Ending in a slash is a dir.
      if (substr($url, -1) === '/') {
        $urlsGrouped['dirs'][] = trim($url);
        continue;
      }
      $urlsGrouped['urls'][] = trim($url);
    }
    return $urlsGrouped;
  }

  /**
   * Frequency check.
   *
   * TODO: Not sure if this has to be implemented for MVP.
   */
  public function frequencyCheck() {
    // Store array in a variable.
    // < 10 update array with timestamp and return TRUE
    // >= 10 unset items > 5min
    // Still >= 10 return FALSE (do not update array.)
    // Update array with timestamp and return TRUE.
  }

  /**
   * Uses http client to perform the request to purge.
   *
   * @param string $endpoint
   *   The CDNetworks endpoing (ex. /ccm/purge/ItemIdReceiver).
   * @param array $options
   *   Options prepared for guzzle.
   */
  public function request($endpoint, array $options) {
    $method = 'post';
    try {
      // For the time, only posts are happening.
      $request = $this->httpClient->{$method}(
        $this->baseUri . $endpoint,
        $options
      );

      // Decode the response.
      $response = Json::decode($request->getBody()->getContents());

      // TODO: confirm itemId means a succesful response.
      if (isset($response['itemId'])) {
        if (($this->config->get('verbose_log')) == 1) {
          $this->logger->info('Successful purge ItemID: @itemid. <pre><code>@debug</code></pre>',
          [
            '@itemid' => $response['itemId'],
            '@debug' => print_r($options, TRUE),
          ]);
        }
        else {
          $this->logger->info('Successful purge ItemID: @itemid', ['@itemid' => $response['itemId']]);
        }
        $this->messenger->addMessage($this->t('Successful purge ItemID: @itemid', ['@itemid' => $response['itemId']]));
        return TRUE;
      }

      // TODO: Possibly implement additional error checking.
      if (isset($response['Code'], $response['Message']) && !$response['code']) {
        $this->logger->error('Problem purging CDNetworks Paths: @message', ['@message' => $response['Message']]);
        $this->messenger->addError($this->t('Problem purging CDNetworks Paths: @message', ['@message' => $response['Message']]));
        return FALSE;
      }

      return FALSE;
    }
    catch (RequestException $exception) {
      // Log Any exceptions.
      $this->logger->error('Problem purging CDNetworks Paths: "%error"', ['%error' => $exception->getMessage()]);
      $this->messenger->addError($this->t('Problem purging CDNetworks Paths: "%error"', ['%error' => $exception->getMessage()]));
      return FALSE;
    }
  }

  /**
   * Prepare options for the client.
   *
   * @param array $body
   *   The body that will be turned into JSON.
   */
  private function prepareOptions(array $body) {
    // TODO: Allow overriding longterm with hook.
    if (!empty($body)) {
      // JSON key does some special things in guzzle.
      $options['json'] = $body;
    }
    // Basic auth adds the correct header and base64 encodes it.
    $options['auth'] = [
      $this->username,
      $this->getPassword(),
    ];
    $options['headers'] = [
      'Date' => $this->date,
      'Accept' => 'application/json',
    ];

    return $options;
  }

  /**
   * Gets the password from APIKey and date.
   *
   * As defined in Open API spec.
   * See https://si.cdnetworks.com/v2/index#/apidoc/index.
   *
   * @return string
   *   Base64 encoded authorization token.
   */
  private function getPassword() {
    $hmac = hash_hmac('SHA1', $this->date, $this->apiKey, TRUE);
    $encodedApiKey = base64_encode($hmac);
    return $encodedApiKey;
  }

  /**
   * Gets the date in the format the API wants.
   *
   * TODO: Possibly use Drupal date functions.
   *
   * @return string
   *   Formatted date.
   */
  private function getDate() {
    // This pulled from example CDNetworks code.
    return gmdate('D, d M Y H:i:s') . ' GMT';
  }

  /**
   * Used to validate Configuration.
   *
   * This could be more thorough.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function validateConfiguration() {
    $props = [
      'username',
      'apiKey',
      'baseUri',
    ];

    foreach ($props as $prop) {
      if (empty($this->{$prop})) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Return a KeyValue.
   *
   * @param string $whichConfig
   *   Name of the config in which the key name is stored.
   *
   * @return mixed
   *   Null or string.
   */
  protected function getKeyValue($whichConfig) {
    if (empty($this->config->get($whichConfig))) {
      return NULL;
    }
    $whichKey = $this->config->get($whichConfig);
    $keyValue = $this->keyRepo->getKey($whichKey)->getKeyValue();

    if (empty($keyValue)) {
      return NULL;
    }

    return $keyValue;
  }

}
