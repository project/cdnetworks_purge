<?php

namespace Drupal\cdnetworks_purge\Plugin\Purge\Purger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\cdnetworks_purge\Client\CdnetworksPurgeApiClient;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use Drupal\purge\Plugin\Purge\Purger\PurgerInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CDNetworks purger.
 *
 * @PurgePurger(
 *   id = "cdnetworks_purge",
 *   label = @Translation("CDNetworks Purger"),
 *   description = @Translation("Purger for CDNetworks."),
 *   types = {"tag", "url", "regex"},
 *   multi_instance = FALSE,
 * )
 */
class CdnetworksPurgePurger extends PurgerBase implements PurgerInterface {

  /**
   * CDNetworks API Client.
   *
   * @var \Drupal\cdnetworks_purge\Client\CdnetworksPurgeApiClient
   */
  protected $client;

  /**
   * The settings configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('cdnetworks_purge.client')
    );
  }

  /**
   * Constructs a \Drupal\Component\Plugin\CDNetworksPurger.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The factory for configuration objects.
   * @param \Drupal\cdnetworks_purge\Client\CdnetworksPurgeApiClient $client
   *   CDNetworks API Client for Drupal.
   *
   * @throws \LogicException
   *   Thrown if $configuration['id'] is missing, see Purger\Service::createId.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config, CdnetworksPurgeApiClient $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->config = $config->get('cdnetworks_purge.settings');
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function hasRuntimeMeasurement() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdealConditionsLimit() {
    $limit = $this->config->get('ideal_conditions_limit') ?? 100;
    return (int) $limit;
  }

  /**
   * {@inheritdoc}
   */
  public function routeTypeToMethod($type) {
    $methods = [
      'url'  => 'invalidateUrls',
      'tag' => 'invalidateTags',
      'regex' => 'invalidateRegex',
    ];
    return isset($methods[$type]) ? $methods[$type] : 'invalidate';
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    throw new \LogicException('This should not execute.');
  }

  /**
   * Invalidate a set of urls.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Exception
   */
  public function invalidateUrls(array $invalidations) {
    $urls = [];
    // Limit to 500 to meet CDNetworks API requirement.
    foreach (array_chunk($invalidations, 500, TRUE) as $invalidationBatch) {
      // Set all invalidation states to PROCESSING before kick off purging.
      /* @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
      foreach ($invalidationBatch as $invalidation) {
        $invalidation->setState(InvalidationInterface::PROCESSING);
        $urls[] = $invalidation->getExpression();
      }
      if (empty($urls)) {
        foreach ($invalidationBatch as $invalidation) {
          $invalidation->setState(InvalidationInterface::FAILED);
          throw new \Exception('No url found to purge');
        }
      }

      $invalidationState = $this->invalidateItems('url', $urls);
      $this->updateState($invalidationBatch, $invalidationState);
    }

  }

  /**
   * Invalidate a set of regex urls.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Exception
   */
  public function invalidateRegex(array $invalidations) {
    $urls = [];
    // Set all invalidation states to PROCESSING before kick off purging.
    /* @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
    foreach ($invalidations as $invalidation) {
      $invalidation->setState(InvalidationInterface::PROCESSING);
      $urls[] = $invalidation->getExpression();
    }

    if (empty($urls)) {
      foreach ($invalidations as $invalidation) {
        $invalidation->setState(InvalidationInterface::FAILED);
        throw new \Exception('No regex url found to purge');
      }
    }

    $invalidationState = $this->invalidateItems('regex', $urls);
    $this->updateState($invalidations, $invalidationState);
  }

  /**
   * Invalidate a set of tags.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   *
   * @throws \Exception
   */
  public function invalidateTags(array $invalidations) {
    // API docs say comma seprated list clears tags matching ALL tags.
    // Example 'tag:1,tag:2,tag:3' would clear pages matching all.
    // Safest to send ONE at a time so 'tag:1' gets cleared from matching pages.
    // @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation.
    foreach (array_chunk($invalidations, 1, TRUE) as $invalidationBatch) {
      // Iterate over each tag to clear one at a time.
      foreach ($invalidationBatch as $invalidation) {
        // Most methods expect an array, however...
        // CDNetworks wants one tag at a time to purge.
        $tag = [];
        $tag[] = $invalidation->getExpression();
        // If the Tag is empty, throw an exception.
        if (empty($tag)) {
          $invalidation->setState(InvalidationInterface::FAILED);
          $invalidationState = $this->invalidateItems('tag', $tag);
          $this->updateState($invalidationBatch, $invalidationState);
          continue;
          // TODO: May want to throw an exception here.
          // Example throw new \Exception('No tag found to purge');.
        }
        $invalidation->setState(InvalidationInterface::PROCESSING);
        // Invalidate and update the item state.
        $invalidationState = $this->invalidateItems('tag', $tag);
        $this->updateState($invalidationBatch, $invalidationState);
      }
    }
  }

  /**
   * Invalidate CDetworks cache.
   *
   * @param mixed $type
   *   Type to purge. Options are url, tag, regex.
   * @param string[] $invalidates
   *   A list of items to invalidate.
   *
   * @return int
   *   Returns invalidate items.
   */
  protected function invalidateItems($type = NULL, array $invalidates = []) {
    try {
      $purged = FALSE;
      if ($type === 'url') {
        // Convert internal url to CDN external domain.
        $cdn_url = $this->config->get('cdn_url');
        foreach ($invalidates as $key => $value) {
          $parts = explode('/', $value);
          if ($cdn_url <> $parts[2]) {
            $parts[2] = $cdn_url;
          }
          $invalidates[$key] = implode('/', $parts);
        }
        $purged = $this->client->purgeUrl($invalidates);
      }
      elseif ($type === 'tag') {
        // Convert to string.
        $tag = reset($invalidates);
        $purged = $this->client->purgeTag($tag);
      }
      elseif ($type === 'regex') {
        $purged = $this->client->purgeRegex($invalidates);
      }

      if ($purged) {
        return InvalidationInterface::SUCCEEDED;
      }
      return InvalidationInterface::FAILED;
    }
    catch (\Exception $e) {
      return InvalidationInterface::FAILED;
    }
  }

  /**
   * Update the invalidation state of items.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidator instance.
   * @param int $invalidation_state
   *   The invalidation state.
   */
  protected function updateState(array $invalidations, $invalidation_state) {
    // Update the state.
    foreach ($invalidations as $invalidation) {
      $invalidation->setState($invalidation_state);
    }
  }

}
