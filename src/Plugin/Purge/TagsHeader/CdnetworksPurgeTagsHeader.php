<?php

namespace Drupal\cdnetworks_purge\Plugin\Purge\TagsHeader;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\purge\Plugin\Purge\TagsHeader\TagsHeaderInterface;
use Drupal\purge\Plugin\Purge\TagsHeader\TagsHeaderBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Exports the tag header.
 *
 * @PurgeTagsHeader(
 *   id = "cdnetworkspurgetagsheader",
 *   header_name = "tag",
 * )
 */
class CdnetworksPurgeTagsHeader extends TagsHeaderBase implements TagsHeaderInterface {

  /**
   * The ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config_factory->get('cdnetworks_purge.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(array $tags) {
    // As of Jan 2020, CDNetworks says the tags must be comma separated.
    // Tags must not contain ':'.
    $repl = str_replace(':', '_', implode(',', $tags));
    return $repl;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    // Get setting from config and check if cache tag is enabled.
    if (($this->config->get('cachetag')) == 1) {
      return TRUE;
    }

    return FALSE;
  }

}
