<?php

namespace Drupal\cdnetworks_purge;

/**
 * Definitions for CDNetworks API purge.
 */
interface CdnetworksPurgeApiClientInterface {

  /**
   * CcmItemIdPurge for directory and url purging.
   *
   * Info: CDN api documentation is not publicly available.
   *
   * @param array $urls
   *   An mix of "directory" and normal urls.
   * @param string $urlAction
   *   The urlAction default, delete, expire. See API docs.
   * @param string $dirAction
   *   The dirAction default, delete, expire. See API docs.
   *
   * @return bool
   *   True or False if the purge was successful for not.
   */
  public function purgeUrl(array $urls, $urlAction = 'default', $dirAction = 'default');

  /**
   * UrlRegularPurgeService for regular expression url purging.
   *
   * Info: CDN api documentation is not publicly available.
   *
   * @param array $urls
   *   An array of URLs in Regex format.
   *
   * @return bool
   *   True or False if the purge was successful for not.
   */
  public function purgeRegex(array $urls);

  /**
   * TagPurgeService for tag based purging.
   *
   * Info: Does not exist in API docs yet.
   *
   * @param array $tag
   *   An a single tag. Example: node:1.
   * @param int $action
   *   Action is 0 or 1 from the documentation.
   *
   * @return bool
   *   True or False if the purge was successful for not.
   */
  public function purgeTag(array $tag, $action = 0);

}
