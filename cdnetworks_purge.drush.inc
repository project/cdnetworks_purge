<?php

/**
 * @file
 * Provides drush commands for CDNetworks Purge related operations.
 */

/**
 * Implements hook_drush_command().
 */
function cdnetworks_purge_drush_command() {
  $items = [];
  $items['cdnetworks-purge-purge-urls'] = [
    'description' => 'Purge one or more URLs from CDNetworks.',
    'arguments' => [
      'urls' => 'Comma separate multiple urls. (Ex. https://example.com/one,https://example.com/two',
    ],
    'drupal dependencies' => ['cdnetworks_purge'],
    'aliases' => ['cdnetworks_purge:purge-urls'],
  ];
  return $items;
}

/**
 * Call back function to purge urls from CDNetworks Purge via drush.
 */
function drush_cdnetworks_purge_purge_urls(array $urls) {
  $client = Drupal::service('cdnetworks_purge.client');
  $paths = explode(',', $urls);
  if (!empty($paths)) {
    // TODO: Print user response.
    $client->flush($paths);
  }
}
